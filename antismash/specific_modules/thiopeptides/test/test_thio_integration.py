try: import unittest2 as unittest
except ImportError: import unittest
from helperlibs.bio import seqio
from antismash import utils
from antismash.config import set_config
from antismash.specific_modules.thiopeptides import specific_analysis
from antismash.specific_modules.thiopeptides import html_output as h

class TestIntegration(unittest.TestCase):
    def setUp(self):
        from argparse import Namespace
        conf = Namespace()
        conf.cpus = 1
        set_config(conf)

    def tearDown(self):
        set_config(None)

    def test_nosiheptide(self):
        "Test thiopeptide prediction for nosiheptide - nosM"
        rec = seqio.read(utils.get_full_path(__file__, 'nosi_before_analysis.gbk'))
        self.assertEqual(58, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(60, len(rec.features))
        prepeptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(prepeptides))
        prepeptide = prepeptides[0]
        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]
        self.assertAlmostEqual(1315.3, h._get_monoisotopic_mass(prepeptide))
        self.assertAlmostEqual(1316.5, h._get_molecular_weight(prepeptide))
        self.assertEqual("MDAAHLSDLDIDALEISEFLDESRLEDSEVVAKVMSA", h._get_leader_peptide_sequence(leader))

        self.assertEqual("SCTTCECCCSCSS", h._get_core_peptide_sequence(prepeptide))
        self.assertEqual("26-member", h._get_macrocycle_size(prepeptide))
        self.assertEqual([1240.4, 1258.4, 1276.5, 1294.5, 1312.5, 1330.5], h._get_mature_alternative_weights(prepeptide))
        self.assertEqual('Type I', h._get_core_peptide_type(prepeptide))
        self.assertAlmostEqual(1221.2, h._get_mature_mass(prepeptide))
        self.assertAlmostEqual(1222.4, h._get_mature_mw(prepeptide))
        self.assertEqual('Central ring: pyridine tetrasubstituted (hydroxyl group present); second macrocycle', h._get_core_features(prepeptide))
        self.assertEqual('dealkylation of C-Terminal residue; amidation', h._get_core_peptide_tail_reaction(prepeptide))

    def test_lactazole(self):
        "Test thiopeptide prediction for lactazole - lazA"
        rec = seqio.read(utils.get_full_path(__file__, 'lac_before_analysis.gbk'))
        self.assertEqual(22, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(24, len(rec.features))
        prepeptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(prepeptides))
        prepeptide = prepeptides[0]
        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]
        self.assertAlmostEqual(1362.5, h._get_monoisotopic_mass(prepeptide))
        self.assertAlmostEqual(1363.5, h._get_molecular_weight(prepeptide))
        self.assertEqual("MSDITASRVESLDLQDLDLSELTVTSLRDTVALPENGA", h._get_leader_peptide_sequence(leader))

        self.assertEqual("SWGSCSCQASSSCA", h._get_core_peptide_sequence(prepeptide))
        self.assertEqual('', h._get_macrocycle_size(prepeptide))
        self.assertEqual([1381.5, 1399.5, 1417.5, 1435.5, 1453.6, 1471.6], h._get_alternative_weights(prepeptide))
        self.assertEqual('Type III', h._get_core_peptide_type(prepeptide))
        self.assertEqual('Central ring: pyridine trisubstituted' ,h._get_core_features(prepeptide))
        self.assertEqual('QPQDM', h._get_tail_cut(prepeptide))

        
    def test_thiostrepton(self):
        "Test thiopeptide prediction for thiostrepton"
        rec = seqio.read(utils.get_full_path(__file__, 'thiostrepton_before_analysis.gbk'))
        self.assertEqual(31, len(rec.features))

        specific_analysis(rec, None)
        self.assertEqual(33, len(rec.features))
        prepeptides = h._find_core_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(prepeptides))
        prepeptide = prepeptides[0]
        leaders = h._find_leader_peptides(utils.get_cluster_by_nr(rec, 1), rec)
        self.assertEqual(1, len(leaders))
        leader = leaders[0]
        self.assertAlmostEqual(1639.6, h._get_monoisotopic_mass(prepeptide))
        self.assertAlmostEqual(1640.9, h._get_molecular_weight(prepeptide))
        self.assertEqual("MSNAALEIGVEGLTGLDVDTLEISDYMDETLLDGEDLTVTM", h._get_leader_peptide_sequence(leader))

        self.assertEqual("IASASCTTCICTCSCSS", h._get_core_peptide_sequence(prepeptide))
        self.assertEqual("26-member", h._get_macrocycle_size(prepeptide))
        self.assertEqual([1664.8, 1682.8, 1700.8, 1718.8, 1736.9, 1754.9, 1772.9, 1790.9], h._get_mature_alternative_weights(prepeptide))
        self.assertEqual('Type II', h._get_core_peptide_type(prepeptide))
        self.assertAlmostEqual(1645.5, h._get_mature_mass(prepeptide))
        self.assertAlmostEqual(1646.8, h._get_mature_mw(prepeptide))
        self.assertEqual('Central ring: piperidine; second macrocycle containing a quinaldic acid moiety', h._get_core_features(prepeptide))
      

