# antiSMASH container with a snapshot of the development tree
# VERSION 0.0.3
FROM debian:jessie
MAINTAINER Kai Blin <kblin@biosustain.dtu.dk>

ENV ANTISMASH_VERSION="dev"

# set up apt-via-https
RUN apt-get update && apt-get install -y apt-transport-https && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# set up antiSMASH deb repo
ADD https://dl.secondarymetabolites.org/antismash.list /etc/apt/sources.list.d/antismash.list
ADD https://dl.secondarymetabolites.org/antismash.asc /tmp/
RUN apt-key add /tmp/antismash.asc

# grab all the dependencies
RUN apt-get update && \
    apt-get install -y \
        clustalw \
        curl \
        default-jre-headless \
        diamond \
        fasttree \
        git \
        glimmerhmm \
        hmmer \
        hmmer2 \
        hmmer2-compat \
        mafft \
        meme-suite \
        muscle \
        ncbi-blast+ \
        prodigal \
        python-backports.lzma \
        python-bcbio-gff \
        python-ete2 \
        python-excelerator \
        python-biopython \
        python-helperlibs \
        python-matplotlib \
        python-networkx \
        python-pandas \
        python-pyquery \
        python-pysvg \
        python-scipy \
        python-sklearn \
        tigr-glimmer \
    && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# Grab antiSMASH
COPY . /antismash-${ANTISMASH_VERSION}

ADD docker/instance.cfg /antismash-${ANTISMASH_VERSION}/antismash/config/instance.cfg

# compress the shipped profiles
RUN hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/abmotifs.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/dockingdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/ksdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/specific_modules/nrpspks/nrpspksdomains.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/generic_modules/smcogs/smcogs.hmm && \
    hmmpress /antismash-${ANTISMASH_VERSION}/antismash/generic_modules/active_site_finder/hmm/p450.hmm3

ADD docker/run /usr/local/bin/run

WORKDIR /usr/local/bin
RUN ln -s /antismash-${ANTISMASH_VERSION}/run_antismash.py

RUN mkdir /matplotlib && MPLCONFIGDIR=/matplotlib python -c "import matplotlib.pyplot as plt" && chmod -R a+rw /matplotlib

VOLUME ["/input", "/output", "/databases"]
WORKDIR /output

ENTRYPOINT ["/usr/local/bin/run"]
